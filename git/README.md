Pending
=======

 * Create "incoming" alias by:
    1. Finding all local branches.
    1. Finding each local branch's remote tracking branch.
    1. Performing a log range query (preferably with --pretty=oneline) with the former as the range start and the latter as the end.

 * Create an "outgoing" alias by:
    1. Performing the same two first steps as in the "incoming" alias.
    1. Performing the same log range query as in the "incoming" alias, but with the boundaries inverted, i.e. the remote tracking branch as the range start and the local branch as the range end.
